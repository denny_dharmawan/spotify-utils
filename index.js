const SpotifyAPI = require('spotify-web-api-node');
const lodash = require('lodash');
const bunyan = require('bunyan');
const ora = require('ora');
const opn = require('opn');

const deepExtractTracksFromPlaylistId = require('./utils/deepExtractTracksFromPlaylistId');

const logger = bunyan.createLogger({name: "spotify-utils"});

(async () => {
  const newPlaylistName = 'My merged playlist';
  const playlistIdsToBeMerged = [
    '49JDu2Cj8wPY8apU2Am47a',
    '37i9dQZF1DWSw3yrhD9FUj',
    '37i9dQZF1DZ06evO1qKOI6',
    '37i9dQZF1DZ06evO1T6jnT',
    '37i9dQZF1DZ06evO3EIbJu',
    '37i9dQZF1DZ06evO3cjKuF'
  ];
  const albumsToBeMerged = [];
  const artistToBeMerged = [
    '3Cv2vi3WTl8VZOTdrBkKdM',
    '13kJgvU22LHMsJtGWLmx7W',
    '55tJwpPIz9BMrSLM45iEXX',
    '3wRA5UYoo08BBKJnzyKkpF',
    '2QHTtUsN6Q13w3QHdfRqsK',
    '503DjcVO5Ku1NgLPhVuNg7',
    '1R52cwGf75yTf7I3Q0Irf8',
    '3uGFTJ7JMllvhgGpumieHF'
  ];
  let tracks = [];

  // Get your token from https://developer.spotify.com/console/get-playlist/
  // Hit get token button
  //
  // Give permissions for:
  // playlist-read-private
  // playlist-read-collaborative
  // playlist-modify-public
  // playlist-modify-private
  //
  // This token expires after one hour, so if there is an error on your request during development
  // try to renew the token

  const token =
    'BQDXJa01C-O_k3L2cwQ0wU6NqfVAIHBciy1YMhqDlUSPuLv8uveikTa2j0bbMNRD_9clDlpMJckMIlloCxblu7lwuF4kiqzuo9uV3Tcw8FW1LYBGO1ENOGYbkrt68gnx-GH_Tjetkus0OIB1ix1bp6ENmQz_y9SkhIZOglgIZsa1xG5x6if2c2SXkDhROgF2zoLobzoyu0PnK2z3Ggus0XZDOa0ORrTK-Qu9E8C0GLcPcZFPOF6h6rH74rU6oBgBL2v7so13';
  const spotify = new SpotifyAPI({});
  spotify.setAccessToken(token);
  const profile = await spotify.getMe();

  const spinner = ora('Processing your request').start();
  // processing playlists
  if (playlistIdsToBeMerged.length !== 0) {
    const playlistTracks = await Promise.all(
      playlistIdsToBeMerged.map(playlistId => deepExtractTracksFromPlaylistId(spotify, playlistId))
    );
    const flattenTracks = lodash.flattenDeep(playlistTracks);
    tracks = tracks.concat(lodash.map(flattenTracks, 'track.uri'));
  }

  // processing albums
  if (albumsToBeMerged.length !== 0) {
    const albums = await spotify.getAlbums(albumsToBeMerged);
    const albumTracks = albums.body.albums.map(album => {
      return album.tracks.items.map(item => {
        return item.uri;
      });
    });
    tracks = tracks.concat(lodash.flattenDeep(albumTracks));
  }

  // processing artists
  if (artistToBeMerged.length !== 0) {
    const artists = await Promise.all(
      artistToBeMerged.map(artistId => {
        return spotify.getArtistTopTracks(artistId, 'ID');
      })
    );
    const artistTracks = artists.map(artist => {
      return artist.body.tracks.map(track => {
        return track.uri;
      });
    });
    tracks = tracks.concat(lodash.flattenDeep(artistTracks));
  }

  // create new playlist
  const newPlaylist = await spotify.createPlaylist(profile.body.id, newPlaylistName, {
    public: false
  });
  const targetPlaylistId = newPlaylist.body.id;
  //const targetPlaylistId = '4lnx4K9c0xbSg06FhglcKv';

  /**
   * Track Refinement:
   * shuffle
   * remove duplicates
   * TODO remove songs with title containing specific keywords e.g. ['Japanese', 'Remix', Inst]
   */
  let refinedTracks = lodash.shuffle(lodash.uniq(tracks));

  // dunno why if tracks are too many, addTracksToPlaylist will throw an error
  const trackChunks = lodash.chunk(refinedTracks, 50);
  for (const trackChunk of trackChunks) {
    await spotify.addTracksToPlaylist(targetPlaylistId, trackChunk);
  }

  spinner.stop();
  await opn('https://open.spotify.com/playlist/' + targetPlaylistId);
})();

process.on('unhandledRejection', reason => {
  logger.error('Unhandled Rejection at:', reason.stack || reason);
});
