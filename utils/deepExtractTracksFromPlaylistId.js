const lodash = require('lodash');

/**
 * spotify api has 100 limit of tracks to be returned, this functions extracts all the tracks
 * from a playlist
 * @param spotify
 * @param playlistId
 * @returns array of extracted tracks
 */
const deepExtractTracksFromPlaylistId = async (spotify, playlistId) => {
  let tracks = [];
  let playlist;

  playlist = await spotify.getPlaylist(playlistId);
  tracks = tracks.concat(playlist.body.tracks.items);

  const numberOfTracks = playlist.body.tracks.total;
  if (numberOfTracks <= 100) {
    return tracks;
  }

  const numberOfNextTracks = numberOfTracks - 100;
  const playlistPageNumbers = lodash.range(Math.ceil(numberOfNextTracks / 100));

  const nextTracks = await Promise.all(
    playlistPageNumbers.map(async pageNumber => {
      const tracks = await spotify.getPlaylistTracks(playlistId, {
        offset: (pageNumber + 1) * 100,
        limit: 100,
        fields: 'items'
      });

      return tracks.body.items;
    })
  );

  return tracks.concat(nextTracks.flat(1));
};

module.exports = deepExtractTracksFromPlaylistId;
