# **Spotify-utils**
A simple script to generate a new playlist by combining several track sources:

- Multiple playlists
- Multiple albums
- Artist top 10 tracks

Furthermore, we also add some refinement to the resulting playlist:

- Shuffle the playlist's tracks
- Remove track duplication on the playlist

### **Fun fact about shuffle**

Spotify shuffling algorithm is not so random:

- [Spotify shuffling algorithm](https://labs.spotify.com/2014/02/28/how-to-shuffle-songs/)
- [User request as of 2020](https://community.spotify.com/t5/Live-Ideas/All-Platforms-Option-to-have-a-true-shuffle/idi-p/4880594)
- [Another User request as of 2020](https://community.spotify.com/t5/Implemented-Ideas/Implement-an-actual-shuffle-function/idi-p/27607)

We use Fisher-Yates algorithm implemented by lodash instead

## Authentication
For simplicity, we use [Spotify Interactice Console](https://developer.spotify.com/web-api/console/get-playlist/) to generate token needed by our API request.

You will need to grant the following scope permissions as seen below depending on the which playlist you are trying to modify.

    playlist-read-private
    playlist-read-collaborative
    playlist-modify-public
    playlist-modify-private
    
The token expires every one hour.

## Variables
You need to modify index.js input variables to specify what you want:

- newPlaylistName
- playlistIdsToBeMerged
- albumsToBeMerged
- artistToBeMerged

 
